import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'de.ngbasics.app',
  appName: 'angular-forms',
  webDir: 'dist',
  server: {
    androidScheme: 'https'
  }
};

export default config;
