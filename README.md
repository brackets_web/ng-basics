# About

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.2.1.

## Developmenten and run server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.
Run mock: `npm run mock` https://github.com/typicode/json-server

# Project

TODO:
[ ] add table component
[ ] add gitflow / githooks => CI/CD
[x] add transloco
[x] add material
[ ] add tailwind
[ ] add api structure => from openAPI to json to fake data
[x] add eslint
[ ] add rxjs
[ ] add rxjsdb / componentStore
[x] init develop
[x] develop as default branch
[x] git flow init
[ ] project organization
[x] ci/cd base
[ ] ci/cd multi platforms
[ ] gitlab tasks init
[ ] gitlab check git rebase strategy
[ ] git hooks
[ ] #BUG to gitlab bug
[ ] #TODO to gitlab task
[ ] #FIXME to gitlab task as something for later to refactor
[ ] gitpod work setup gitpod.yml
[ ] vscode setup .vscode/
[x] prettierrc
[x] eslint init - sorts - formatting
[ ] app organization
[ ] ui screens
[ ] ux plan
[ ] testing first
[ ] init sqlite
[ ] seeding data over interfaces & faker
[ ] add compodoc
[ ]
[ ]
[ ]
[ ]
[ ]
[ ]
