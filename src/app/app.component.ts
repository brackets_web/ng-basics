import { SpielwieseService } from 'src/app/services/spielwiese.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `<router-outlet></router-outlet>`,
})
export class AppComponent {
  constructor(private spielwieseService: SpielwieseService) {
    this.spielwieseService.getProfile().subscribe((res) => {
      console.log('res', res);
    });
  }
}
