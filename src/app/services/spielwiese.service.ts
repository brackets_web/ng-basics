import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, shareReplay, tap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SpielwieseService {
  constructor(private http: HttpClient) { }

  getProfile(): Observable<any> {
    return this.http.get('http://localhost:5000/profile');
  }

  getJsCodeFromServer(): Observable<any> {
    return of({ code: 'console.log("Hello World!")', cont: Math.random() }).pipe(
      tap(res => {
        console.log('res', res);
      }),
      shareReplay({ bufferSize: 1, refCount: true }),
    );
    // return this.http.get('http://localhost:3000/js-code');
  }
}
