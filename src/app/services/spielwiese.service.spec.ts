/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SpielwieseService } from './spielwiese.service';

describe('Service: Spielwiese', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SpielwieseService]
    });
  });

  it('should ...', inject([SpielwieseService], (service: SpielwieseService) => {
    expect(service).toBeTruthy();
  }));
});
